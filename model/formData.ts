import { FormDataType } from '../enum/formDataType'
import { Data } from './envelope'

export class FormData extends Data {
    fieldName!: string
    type!: FormDataType
    value!: string | number
    options?: string[]
}