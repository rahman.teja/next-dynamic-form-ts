export class Data {

}

export class Envelope<T extends Data> {
    success!: boolean
    message!: string
    data!: T[]
}