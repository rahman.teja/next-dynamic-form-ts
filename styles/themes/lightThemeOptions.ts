import { ThemeOptions } from "@mui/material/styles";

const options: ThemeOptions = {
  palette: {
    mode: "light",
  },
};

export default options;
