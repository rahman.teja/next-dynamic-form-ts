import { GetStaticProps, InferGetStaticPropsType } from 'next'
import type { NextPage } from 'next'
import React from 'react'
import { Container, Button, TextField, Box, MenuItem } from '@mui/material'
import { Envelope, FormData } from "../model"
import { FormDataType } from "../enum/formDataType"

export const getStaticProps: GetStaticProps = async (context) => {
    const res = await fetch('https://ulventech-react-exam.netlify.app/api/form')
    const result: Envelope<FormData> = await res.json()
    const datas: FormData[] = result.data

    return {
        props: {
            datas,
            message: '',
        },
    }
}

const Form: NextPage = ({
    datas,
    msg,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
    const [formList, setFormList] = React.useState(datas)
    const [message, setMessage] = React.useState(msg)

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        let data: {
            [key: string]: any
        } = {};

        formList.forEach((f: FormData) => {
            data[f.fieldName] = f.value
        })

        const postResult = await fetch('https://ulventech-react-exam.netlify.app/api/form', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        const result = await postResult.json()

        setMessage(JSON.stringify(result))
    };

    const handleChange = (val: string | number, i: number) => {
        console.log(val, 'i', i)
        formList[i].value = val
        setFormList(formList)
    }

    if (!formList) return <h1>Loading...</h1>

    return (
        <Container>
            <h1>Dynamic Form</h1>
            <Box component="form" onSubmit={handleSubmit}>
                {formList.map((form: FormData, i: number) => (
                    <TextField
                        key={form.type + '-' + i}
                        required
                        fullWidth
                        id={form.type + '-' + i}
                        label={form.fieldName}
                        defaultValue={form.value}
                        select={form.type === FormDataType.SELECT}
                        multiline={form.type === FormDataType.MULTILINE}
                        rows={4}
                        margin="normal"
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleChange(event.target.value, i)}
                    >
                        {form.options?.map((opt: string, i: number) => (
                            <MenuItem
                                key={'select-opt-' + opt + '-' + i}
                                value={opt}
                            >
                                {opt}
                            </MenuItem>
                        ))}
                    </TextField>
                ))}
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Submit
                </Button>
            </Box>
            <div>
                {message}
            </div>
        </Container>
    );
}

export default Form