import { TextField } from '@mui/material'

type Props = {
    id: string
    label: string
    disabled?: boolean
    defaultValue: string | number
}

export const Text: React.FC<Props> = (prop: Props) => {
    return (
        <TextField
          required
          id={prop.id}
          label={prop.label}
          defaultValue={prop.defaultValue}
        />
    )
}