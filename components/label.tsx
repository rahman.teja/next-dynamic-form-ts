type Props = {
    label: String
}

export const Label: React.FC<Props> = (prop: Props) => {
    return (
        <label>{prop.label}</label>
    )
}