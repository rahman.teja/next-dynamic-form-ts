export enum FormDataType {
    TEXT = "text",
    EMAIL = "email",
    NUMBER = "number",
    MULTILINE = "multiline",
    SELECT = "select",
}